
import { SkTheme } from "../../sk-core/src/sk-theme.js";


export class JqueryTheme extends SkTheme {

    get basePath() {
        if (! this._basePath) {
            this._basePath = (this.configEl && typeof this.configEl.hasAttribute === 'function'
                && this.configEl.hasAttribute('theme-path'))
                ? `${this.configEl.getAttribute('theme-path')}` : '/node_modules/sk-theme-jquery';
        }
        return this._basePath;
    }

    get styles() {
        if (! this._styles) {
            this._styles = {
                'jquery-ui.css': `${this.basePath}/jquery-ui.css`,
                'theme.css': `${this.basePath}/theme.css`,
                'jquery-theme.css': `${this.basePath}/jquery-theme.css`
            };
        }
        return this._styles;
    }

}